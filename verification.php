<?php

function bv_verification() {
    $url = 'https://api.shuftipro.com/';
    $client_id = 'i22SYlgu0BtmswKu9ouLlsP7yOQN4Lj4h36wuud6nbDiqoGFXK1643090758';
    $secret_key = '$2y$10$Vl4/aqOV8Lno6FkIY6sec.lRt/mQJadCS0lxXKdNWFdBEEcmJOGRC';

    $verification_request = [
        'reference' => 'ref-' . rand(4, 444) . rand(4, 444),
        'callback_url' => 'https://dev.petsforhomes.com.au/breeder-verification-result',
        'email' => 'kennyalmendral.sandbox1@gmail.com',
        'country' => 'PH',
        'verification_mode' => 'image_only'
    ];

    $verification_request['document'] = [
        'proof' => '',
        'additional_proof' => '',
        'supported_types' => ['id_card', 'credit_or_debit_card', 'driving_license', 'passport'],
        'dob' => '1993-01-27',
        'age' => '29',
        'document_number' => 'ABC1234XYZ098',
        'issue_date' => '2020-01-10',
        'expiry_date' => '2026-02-01',
        'gender' => 'M',
        'fetch_enhanced_data' => '1',
        'name' => [
            'first_name' => 'John',
            'last_name' => 'Doe'
        ],
        'backside_proof_required' => "1"
    ];

    $verification_request['face'] = [
        'proof' => '',
        'allow_offline' => '0',
        'allow_online' => '0'
    ];

    $auth = "$client_id:$secret_key";
    $headers = ['Content-Type: application/json'];
    $post_data = json_encode($verification_request);
    $response = send_curl($url, $post_data, $headers, $auth);

    $exploded = explode("\n", $response['headers']);
    $sp_signature = null;

    foreach ($exploded as $key => $value) {
        if (strpos($value, 'signature: ') !== false || strpos($value, 'Signature: ') !== false) {
            $sp_signature = trim(explode(':', $exploded[$key])[1]);
            break;
        }
    }

    $calculate_signature  = hash('sha256', $response['body'] . $secret_key);
    $decoded_response = json_decode($response['body'], true);
    $event_name = $decoded_response['event'];

    if ($event_name == 'request.pending') {
        if ($sp_signature == $calculate_signature) {
            $verification_url = $decoded_response['verification_url']; 

            return '<iframe src="'. $verification_url . '" id="shuftipro-iframe" width="100%" height="520" allow="camera ' . $verification_url . '" frameborder="0"></iframe>';
        } else {
            return "Invalid signature: " . print_r($response, true);
        }
    } else {
        return "Error: " . $response;
    }
}

add_shortcode('bv_verification', 'bv_verification');

function bv_verification_webhook() {
    if ($json = json_decode(file_get_contents("php://input"), true)) {
        return send_test_mail(print_r($json, true));
    }

    return false;
}

add_shortcode('bv_verification_webhook', 'bv_verification_webhook');

?>