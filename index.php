<?php

/**
 * Plugin Name: Breeder Verification
 * Description: Handles the verification process for P4H breeders.
 * Version: 0.0.1
 * Author: Pets for Homes
 * Author URI: https://petsforhomes.com.au
 */

require_once 'verification.php';

function mailtrap($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.mailtrap.io';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 2525;    
    $phpmailer->Username = '56477bf8b9efeb';
    $phpmailer->Password = '6aa34e3994ba51';
}

add_action('phpmailer_init', 'mailtrap');

function send_test_mail($message) {
    $to = 'kennyalmendral.sandbox1@gmail.com';
    $subject = 'The subject';
    $body = "<pre>$message</pre>";
    $headers = array(
        'Content-Type: text/html; charset=UTF-8',
        'From: No reply <no-reply@test.com>'
    );
     
    wp_mail($to, $subject, $body, $headers);
}

// add_action('admin_init', 'send_test_mail');

function on_mail_error($wp_error) {
    echo "<pre>";
    print_r($wp_error);
    echo "</pre>";

    die(0);
}

add_action('wp_mail_failed', 'on_mail_error', 10, 1);

function send_curl($url, $post_data, $headers, $auth) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_USERPWD, $auth);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

    $html_response = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $headers = substr($html_response, 0, $header_size);
    $body = substr($html_response, $header_size);

    curl_close($ch);

    return [
        'headers' => $headers,
        'body' => $body
    ];
}

?>